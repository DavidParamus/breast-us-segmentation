import shutil
import cv2
import os

# height x Weight
target_size = (360, 360)

array_of_img = []  # this if for store all of the image data


def move_img(Breast_type, read_dir, write_dir):
    print("[INFO] Copy \"{}\" from \"{}\" into \"{}\"... ".format(Breast_type, read_dir, write_dir))
    target_table = os.path.join(
        read_dir, 'Breast_{}_tb.csv'.format(Breast_type))
    print("[INFO] Opening target table \"{}\"...".format(target_table))
    f = open(target_table, "r")
    row_datas = f.readlines()
    f.close()
    row_datas = row_datas[1:]
    for row_data in row_datas:
        data = row_data.replace("\n", "").split(",")
        if int(data[-1]) != 0:
            # Origin file
            train_dir = os.path.join(write_dir, "{}_Train".format(Breast_type))
            if not os.path.exists(train_dir):
                os.makedirs(train_dir)
            shutil.copyfile(os.path.join(read_dir, "{}".format(Breast_type), data[-3]),
                            os.path.join(train_dir, data[-3]))
            # Label file
            label_dir = os.path.join(write_dir, "{}_Label".format(Breast_type))
            if not os.path.exists(label_dir):
                os.makedirs(label_dir)
            shutil.copyfile(os.path.join(read_dir, "{}_label".format(Breast_type), data[-2]),
                            os.path.join(label_dir, data[-2]))
                 # Label file
            label_check_dir = os.path.join(write_dir, "{}_Label_check".format(Breast_type))
            if not os.path.exists(label_check_dir):
                os.makedirs(label_check_dir)
            shutil.copyfile(os.path.join(read_dir, "{}_label_check".format(Breast_type),data[-2]),
                            os.path.join(label_check_dir, data[-2]))


def resize_Breast_img(read_directory, write_directory, last_index):
    weight, height = target_size
    # this loop is for read each image in this foder,read_directory is the foder name with images.
    filenames = os.listdir(r"./"+read_directory)
    if not os.path.exists(write_directory):
        os.makedirs(write_directory)
    for index, filename in enumerate(filenames):
        # img is used to store the image data
        name_split = filename.split('.')
        img = cv2.imread(read_directory + "/" + filename)
        array_of_img.append(img)
        img_name = "%05d_%s.png" % (
            last_index + index, name_split[0])
        img = cv2.resize(img, (weight+20, height+20),
                         interpolation=cv2.INTER_CUBIC)
        img = img[10:(height+10), 10:(weight+10)]
        #cv2.imshow(imgName, img)
        cv2.imwrite(write_directory+img_name, img)
    # cv2.waitKey(0)
    return last_index + len(filenames)


def resize_label_img(read_directory, write_directory, last_index):
    weight, height = target_size
    # this loop is for read each image in this foder,read_directory is the foder name with images.
    filenames = os.listdir(r"./"+read_directory)
    if not os.path.exists(write_directory):
        os.makedirs(write_directory)
    for index, filename in enumerate(filenames):
        # img is used to store the image data
        name_split = filename.split('.')
        img = cv2.imread(read_directory + "/" + filename)
        array_of_img.append(img)
        img_name = "%05d_%s.png" % (
            last_index + index, name_split[0])
        img = cv2.resize(img, (weight+20, height+20),
                         interpolation=cv2.INTER_NEAREST)
        img = img[10:(height+10), 10:(weight+10)]
        #cv2.imshow(imgName, img)
        cv2.imwrite(write_directory+img_name, img)
    # cv2.waitKey(0)
    return last_index + len(filenames)


def resize_train_and_label():
    '''
        Move database data to r'Dataset_Resize/Origin/*'
    '''
    move_img('Benign', r"../Matlab/Database/", r"./Dataset_resize/Origin/")
    move_img('Malignant', r"../Matlab/Database/", r"./Dataset_resize/Origin/")

    '''
        Benign 良性
    '''
    print("[INFO] Resize resolution: {}".format(target_size))
    image_index = 1
    label_index = 1
    check_index = 1

    image_index = resize_Breast_img(r"./Dataset_resize/Origin/Benign_Train",
                                    r"./Dataset_resize/Resize/Resize_Image/", image_index)
    label_index = resize_label_img(r"./Dataset_resize/Origin/Benign_Label",
                                   r"./Dataset_resize/Resize/Resize_Label/", label_index)
    check_index = resize_Breast_img(r"./Dataset_resize/Origin/Benign_Label_check",
                                    r"./Dataset_resize/Resize/Resize_Label_check/", check_index)

    # #  清空 img array
    array_of_img.clear()

    '''
        Malignant 惡性
    '''
    image_index = resize_Breast_img(r"./Dataset_resize/Origin/Malignant_Train",
                                    r"./Dataset_resize/Resize/Resize_Image/", image_index)
    label_index = resize_label_img(r"./Dataset_resize/Origin/Malignant_Label",
                                   r"./Dataset_resize/Resize/Resize_Label/", label_index)
    check_index = resize_Breast_img(r"./Dataset_resize/Origin/Malignant_Label_check",
                                    r"./Dataset_resize/Resize/Resize_Label_check/", check_index)


if __name__ == '__main__':
    resize_train_and_label()
