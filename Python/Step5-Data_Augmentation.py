import Augmentor
import cv2
import random
import shutil
import os


def gen_train_and_test(valiation_percent, image_src, label_src, label_check_src, write_dir, isAugment=True):
    print("[DEBUG] \"{}\": write_dir : {}".format("gen_train_and_test", write_dir))
    
    # write dir:
    train_image_dir = os.path.join(write_dir, "Train_image/")
    train_label_dir = os.path.join(write_dir, "Train_label/")
    # test_image_dir = os.path.join(write_dir, "Test_image/")
    # test_label_dir = os.path.join(write_dir, "Test_label/")
    # test_check_dir = os.path.join(write_dir, "Test_label_check/")
    target_dirs = [train_image_dir, train_label_dir]
                #    test_image_dir, test_label_dir, test_check_dir]
    for target_dir in target_dirs:
        if not os.path.exists(target_dir):
            print("[INFO] Create dir : {}".format(target_dir))
            os.mkdir(target_dir)

    images = os.listdir(image_src)
    labels = os.listdir(label_src)
    label_checks = os.listdir(label_check_src)
    
    isManual = False
    target_list = []
    # if isManual == False:
    #     benign_amount = len(os.listdir("./Dataset_resize/Origin/Benign_Train"))
    #     malignant_amount = len(os.listdir("./Dataset_resize/Origin/Malignant_Train"))
    #     benign_num = int(benign_amount * (valiation_percent/100))
    #     malignant_num = int(malignant_amount * (valiation_percent/100))
    #     print("[DEBUG] Total : {} ; Test amount: {} (B:{};M:{})".format(len(images), benign_num + malignant_num, benign_num, malignant_num))

    #     benign_list = list(range(0, benign_amount))
    #     # print("[DEBUG] Benign list : {} ".format(benign_list))
    #     random.shuffle(benign_list)
    #     benign_list = benign_list[:benign_num]

    #     malignant_list = list(range(benign_amount, len(images)))
    #     # print("[DEBUG] Malignant list : {} ".format(malignant_list))
    #     random.shuffle(malignant_list)
    #     malignant_list = malignant_list[:malignant_num]
        

    #     target_list = benign_list + malignant_list
    #     print("[DEBUG] Target list : {} ".format(target_list))
    # else: 
    #     # Pick by DB index
    #     target_list = [ 8,   9,   34,  35,  163,
    #                     211, 212, 286, 303, 325 ]
    #     # Convert to list index
    #     target_list = [x-1 for x in target_list]
    
    for index in range(len(images)):
        if index in target_list:
            shutil.copyfile(os.path.join(image_src, images[index]), os.path.join(
                test_image_dir, images[index]))
            shutil.copyfile(os.path.join(label_src, labels[index]), os.path.join(
                test_label_dir, images[index]))
            shutil.copyfile(os.path.join(label_check_src, label_checks[index]), os.path.join(
                test_check_dir, images[index]))
        else:
            shutil.copyfile(os.path.join(image_src, images[index]), os.path.join(
                train_image_dir, images[index]))
            shutil.copyfile(os.path.join(label_src, labels[index]), os.path.join(
                train_label_dir, images[index]))
    
    print("[INFO] Please check the result.")
    is_continue = input('Do you want to continue the data augmentation? [Y/N] ')
    is_continue = is_continue.upper()
    while is_continue != 'Y' and is_continue != 'N':
        is_continue = input('Do you want to continue the data augmentation? [Y/N] ')
        is_continue = is_continue.upper()
    
    if is_continue == 'Y':
        print("[INFO] Starting data augmentation...")
        if isAugment:
            data_augmentation(train_image_dir, train_label_dir, "Train", write_dir)
            # data_augmentation(test_image_dir, test_label_dir, "Test", write_dir)
            # split_train_and_validation(valiation_percent, write_dir)
    elif is_continue == 'N':
        print("[INFO] Ignore data augmentation operation. Erasing split dataset...")
        for target_dir in target_dirs:
            print("[INFO] Deleting dataset: {}".format(target_dir))
            shutil.rmtree(target_dir)
        print("[INFO] Delete all dataset complete")

def data_augmentation(img_src, label_src, target_type, write_dir):
    
    target_num = int(len(os.listdir(img_src))) * 20
    print("[INFO] Data augmentation  to : {} ".format(target_num))
    p = Augmentor.Pipeline(img_src, target_type+"_image_Aug/")

    # Point to a directory containing ground truth data.
    # Images with the same file names will be added as ground truth data
    # and augmented in parallel to the original data.
    p.ground_truth(label_src)
    # Add operations to the pipeline as normal:
    p.rotate(probability=1, max_left_rotation=5, max_right_rotation=5) #旋轉
    p.flip_left_right(probability=0.75) #按概率左右翻轉
    p.zoom_random(probability=0.6, percentage_area=0.75) #隨即將一定比例面積的圖形放大至全圖
    p.flip_top_bottom(probability=0.75) #按概率隨即上下翻轉
    p.random_distortion(probability=0.5,grid_width=10,grid_height=10, magnitude=20) #小塊變形
    p.sample(target_num)
    
    if target_type == "Train":
        aug_src_path = os.path.join(img_src, "Train_image_Aug")
        aug_t_image_dir = os.path.join(write_dir, "Train_image_Aug/")
        aug_t_label_dir = os.path.join(write_dir, "Train_label_Aug/")
    elif target_type == "Test":
        aug_src_path = os.path.join(img_src, "Test_image_Aug")
        aug_t_image_dir = os.path.join(write_dir, "Test_image_Aug/")
        aug_t_label_dir = os.path.join(write_dir, "Test_label_Aug/")
    target_dirs = [aug_t_image_dir, aug_t_label_dir]

    for target_dir in target_dirs:
        print("[DEBUG] write_dir : {} : {}".format(
            target_dir, os.path.exists(target_dir)))
        if not os.path.exists(target_dir):
            print("[INFO] Create dir : {}".format(target_dir))
            os.mkdir(target_dir)

    print("[INFO] Moving image to data: {}; label: {}".format(
       aug_src_path, aug_t_label_dir))
    
    # Move augment data to the different folder
    aug_datas = os.listdir(aug_src_path)
    for aug_data in aug_datas:
        if aug_data.find("_groundtruth_") != -1:
            shutil.move(os.path.join(aug_src_path, aug_data),
                        os.path.join(aug_t_label_dir, aug_data))
        else:
            shutil.move(os.path.join(aug_src_path, aug_data),
                        os.path.join(aug_t_image_dir, aug_data))
    shutil.rmtree(aug_src_path)
    print("[INFO] Moving augmentaion images completed!")

def split_train_and_validation(percent, write_dir):
    print("[DEBUG] \"{}\": write_dir : {}".format(
        "split_train_and_validation", write_dir))
    aug_t_image_dir = os.path.join(write_dir, "Train_image_Aug/")
    aug_t_label_dir = os.path.join(write_dir, "Train_label_Aug/")

    # Create validation folder 
    validation_image_dir = os.path.join(write_dir, "validation_image_Aug/")
    validation_label_dir = os.path.join(write_dir, "validation_label_Aug/")
    target_dirs = [validation_image_dir, validation_label_dir]
    for target_dir in target_dirs:
        print("[DEBUG] write_dir : {} : {}".format(
            target_dir, os.path.exists(target_dir)))
        if not os.path.exists(target_dir):
            print("[INFO] Create dir : {}".format(target_dir))
            os.mkdir(target_dir)


    # split training and valivation data  
    images = os.listdir(aug_t_image_dir)
    labels = os.listdir(aug_t_label_dir)
    augment_num = int(len(images) * (percent/100))
    print("[DEBUG] len(images) : %d" % (len(images)))
    target_list = list(range(0, len(images)))
    random.shuffle(target_list)
    target_list = target_list[:augment_num]
    print("[DEBUG] len(target_list) : %d" % (len(target_list)))
    for index in target_list:
        shutil.move(os.path.join(aug_t_image_dir, images[index]),
                    os.path.join(validation_image_dir, images[index]))
        shutil.move(os.path.join(aug_t_label_dir, labels[index]),
                    os.path.join(validation_label_dir, labels[index]))


if __name__ == "__main__":
    gen_train_and_test(10,
                       r"./Dataset_resize/Resize/Resize_Image/",
                       r"./Dataset_resize/Resize/Resize_Label/",
                       r"./Dataset_resize/Resize/Resize_Label_check/",
                       r"./Dataset/")
