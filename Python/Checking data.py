f = open("../Matlab/Database/Breath_Benign_tb.csv", "r")
row_datas = f.readlines()
f.close()

selected = []

print("\n\nrow_datas: len: %d" % (len(row_datas)))
print("--------------------------------------")
for row_data in row_datas:
    row_data = row_data.replace("\n","")
    print(row_data)
    data = row_data.split(",")
    if data[-1] == '1':
        selected.append(row_data)

print("\n\nselected: len: %d" % (len(selected)))
print("--------------------------------------")
for select in selected:
    print(select)
