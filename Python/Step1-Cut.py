import cv2
import os

benign_integrate = r"./Dataset_cut/Cut Result/Benign_All/"
malignant_integrate = r"./Dataset_cut/Cut Result/Malignant_All/"
#os.makedirs(benign_integrate)
#os.makedirs(malignant_integrate)

def cut_imgs(read_directory, write_directory, cut_range):
    #os.makedirs(write_directory)
    imgs = []
    x, y, w, h = cut_range
    for index, filename in enumerate(os.listdir(r"./"+read_directory)):
        # img is used to store the image data
        #print("{}: {}".format(index, filename))
        img = cv2.imread(read_directory + "/" + filename, cv2.IMREAD_COLOR)
        imgs.append(img)
        img_name = "%s" % (filename)
        img = img[y:y+h, x:x+w]
        #cv2.imshow(filename, img)
        cv2.imwrite(write_directory+img_name, img)
        if write_directory.find("Benign") != -1:
            cv2.imwrite(benign_integrate+img_name, img)
        elif write_directory.find("Malignant") != -1:
            cv2.imwrite(malignant_integrate+img_name, img)
            
    return imgs

def special_cut_img(read_dir, filename, writed_dir, cut_range):
    #os.makedirs(read_dir)
    x, y, w, h = cut_range
    img = cv2.imread(os.path.join(read_dir + "/" + filename), cv2.IMREAD_COLOR)
    print(os.path.join(read_dir, filename))
    img = img[y:y+h, x:x+w]
    cv2.imshow(filename, img)
    cv2.imwrite(writed_dir+filename, img)
    if writed_dir.find("Benign") != -1:
        cv2.imwrite(benign_integrate+filename, img)
    elif writed_dir.find("Malignant") != -1:
        cv2.imwrite(malignant_integrate+filename, img)
            
def main():
    #cut_imgs(r"./Run_result/Run_Result/", r"./Run_result/Run_result_Cut/", (1500, 70, 2100, 1280))
    
    '''
        Benign - ATL3000
    '''
    cut_imgs(r"./Dataset_cut/ATL3000_Benign/3.0/",
             r"./Dataset_cut/Cut Result/ATL3000_Benign/3.0/", (135, 60, 410, 360))
    cut_imgs(r"./Dataset_cut/ATL3000_Benign/4.0/",
             r"./Dataset_cut/Cut Result/ATL3000_Benign/4.0/", (170, 60, 345, 368))
    cut_imgs(r"./Dataset_cut/ATL3000_Benign/4.8/",
             r"./Dataset_cut/Cut Result/ATL3000_Benign/4.8/", (190, 65, 290, 360))
    cut_imgs(r"./Dataset_cut/ATL3000_Benign/6.0/",
             r"./Dataset_cut/Cut Result/ATL3000_Benign/6.0/", (220, 65, 235, 360))
    '''
        Benign - ATL5000
    '''
    cut_imgs(r"./Dataset_cut/ATL5000_Benign/3.0/",
             r"./Dataset_cut/Cut Result/ATL5000_Benign/3.0/", (136, 96, 411, 291))
    cut_imgs(r"./Dataset_cut/ATL5000_Benign/3.9/",
             r"./Dataset_cut/Cut Result/ATL5000_Benign/3.9/", (135, 55, 410, 375))
    cut_imgs(r"./Dataset_cut/ATL5000_Benign/4.0/",
             r"./Dataset_cut/Cut Result/ATL5000_Benign/4.0/", (160, 65, 350, 360))
    cut_imgs(r"./Dataset_cut/ATL5000_Benign/4.8_1/",
             r"./Dataset_cut/Cut Result/ATL5000_Benign/4.8_1/", (160, 55, 375, 360))
    cut_imgs(r"./Dataset_cut/ATL5000_Benign/4.8_2/",
             r"./Dataset_cut/Cut Result/ATL5000_Benign/4.8_2/", (190, 68, 295, 355))
    """
    ''' 
        Bebign - ALT3000 - other
    '''
    special_cut_img(r"./Dataset_cut/ATL3000_Benign/Other/", "lee-byungok64fcd002.bmp",
             r"./Dataset_cut/Cut Result/ATL3000_Benign/Other/", (124, 129, 368, 319))

    """
    '''
        Malignant - ATL3000
    '''
    cut_imgs(r"./Dataset_cut/ATL3000_Malignant/3.0/",
             r"./Dataset_cut/Cut Result/ATL3000_Malignant/3.0/", (138, 60, 410, 365))
    cut_imgs(r"./Dataset_cut/ATL3000_Malignant/4.0/",
             r"./Dataset_cut/Cut Result/ATL3000_Malignant/4.0/", (160, 65, 350, 360))
    cut_imgs(r"./Dataset_cut/ATL3000_Malignant/4.8/",
             r"./Dataset_cut/Cut Result/ATL3000_Malignant/4.8/", (191, 65, 290, 360))
    cut_imgs(r"./Dataset_cut/ATL3000_Malignant/6.0/",
             r"./Dataset_cut/Cut Result/ATL3000_Malignant/6.0/", (219, 65, 235, 360))
    '''
        Maligan - ATL5000
    '''
    cut_imgs(r"./Dataset_cut/ATL5000_Malignant/3.0/",
             r"./Dataset_cut/Cut Result/ATL5000_Malignant/3.0/", (140, 95, 400, 292))
    cut_imgs(r"./Dataset_cut/ATL5000_Malignant/3.9_1/",
             r"./Dataset_cut/Cut Result/ATL5000_Malignant/3.9_1/", (150, 55, 390, 370))
    cut_imgs(r"./Dataset_cut/ATL5000_Malignant/3.9_2/",
             r"./Dataset_cut/Cut Result/ATL5000_Malignant/3.9_2/", (135, 55, 406, 373))
    cut_imgs(r"./Dataset_cut/ATL5000_Malignant/4.8_1/",
             r"./Dataset_cut/Cut Result/ATL5000_Malignant/4.8_1/", (158, 56, 370, 370))
    cut_imgs(r"./Dataset_cut/ATL5000_Malignant/4.8_2/",
             r"./Dataset_cut/Cut Result/ATL5000_Malignant/4.8_2/", (190, 65, 290, 360))


if __name__ == '__main__':
    main()
