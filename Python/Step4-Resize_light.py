import shutil
import cv2
import os


def resize_origin_img(read_directory, write_directory):
    print("[INFO] Resize origin images from \"{}\"...".format(read_directory))
    weight, height = target_size
    # this loop is for read each image in this foder,read_directory is the foder name with images.
    filenames = os.listdir(read_directory)
    if not os.path.exists(write_directory):
        os.makedirs(write_directory)
    # Start resizing images
    for index, filename in enumerate(filenames):
        img = cv2.imread(os.path.join(read_directory, filename))
        img = cv2.resize(img, (weight+20, height+20),
                         interpolation=cv2.INTER_CUBIC)
        img = img[10:(height+10), 10:(weight+10)]
        cv2.imwrite(os.path.join(write_directory, filename), img)
    print("[INFO] Resize complete.\n")


def resize_label_img(read_directory, write_directory):
    print("[INFO] Resize groundtruth marks from \"{}\"...".format(read_directory))
    weight, height = target_size
    # this loop is for read each image in this foder,read_directory is the foder name with images.
    filenames = os.listdir(read_directory)
    if not os.path.exists(write_directory):
        os.makedirs(write_directory)
    for index, filename in enumerate(filenames):
        # img is used to store the image data
        img = cv2.imread(os.path.join(read_directory, filename))
        img = cv2.resize(img, (weight+20, height+20),
                         interpolation=cv2.INTER_NEAREST)
        img = img[10:(height+10), 10:(weight+10)]
        cv2.imwrite(os.path.join(write_directory, filename), img)
    print("[INFO] Resize complete.\n")


if __name__ == '__main__':
    # image height x Weight
    target_size = (360, 360)
    
    # Set origin directory
    ORIGIN_IMAGE_DIR = r"./origin/images"
    OUTPUT_IMAGE_DIR = r"./output/images"
    ORIGIN_GT_MARKS_DIR = r"./origin/labels"
    OUTPUT_GT_MARKS_DIR = r"./output/labels"

    # Start resize
    resize_origin_img(ORIGIN_IMAGE_DIR, OUTPUT_IMAGE_DIR)
    resize_label_img(ORIGIN_GT_MARKS_DIR, OUTPUT_GT_MARKS_DIR)





    
    

