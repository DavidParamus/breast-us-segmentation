import csv
import os
import shutil


def mark_selected(src_dir, filename, selected_index):
    print("[INFO] Marking selected data")
    print("[INFO] Reading data table : {}".format(filename))
    fp = open(os.path.join(src_dir, filename), "r")
    row_datas = fp.readlines()
    fp.close()
    title_row = row_datas[0]
    row_datas = row_datas[1:]
    for index, row_data in enumerate(row_datas):
        try:
            data = row_data.split(",")
            data_index = int(data[0])
            for selected in selected_index:
                if data_index == selected:
                    data[-1] = '1'
                    new_row = ",".join(data)
                    #print("[DEBUG] New_row : {}".format(new_row))
                    row_datas[index] = new_row + "\n"
                    break
        except:
            print("[ERROR] row data is empyt : {}".format(row_data))
    row_datas.insert(0,title_row)
    #check_table(row_datas)
    print("[INFO] Rewriting data table : {}".format(filename))
    fp = open(os.path.join(src_dir, filename), "w")
    fp.writelines(row_datas)
    fp.close()


def write_labelImg_Name(Breast_type, labelImg_dir):
    print("[INFO] Writing {} label images' filename into table... ".format(Breast_type))
    f = open('../Matlab/Database/Breast_{}_tb.csv'.format(Breast_type), "r")
    row_datas = f.readlines()
    f.close()
    title_row = row_datas[0]
    row_datas = row_datas[1:]
    # 讀取 CSV 檔案內容
    for index, row_data in enumerate(row_datas):
        filenames = os.listdir(labelImg_dir)
        for filename in filenames:
            fn_split = filename.replace("\n", "").split("_")
            row_split = row_data.split(",")
            if int(fn_split[0]) == int(row_split[0]):
                row_split[-2] = filename
                new_row = ",".join(row_split)
                row_datas[index] = new_row
                break
    row_datas.insert(0, title_row)
    # check_table(row_data)
    print("[INFO] Saving file to Breast_{}_tb.csv".format(Breast_type))

    f = open('../Matlab/Database/Breast_{}_tb.csv'.format(Breast_type), "w")
    f.writelines(row_datas)
    f.close()
    print("[INFO] Save Completed!!".format(Breast_type))


def selectd_label_check(Breast_type, src_dir, filename, dst_dir):    
    print("[INFO] Moving \"label check\" files to {}...".format(dst_dir))
    print("[INFO] Reading data table : {}".format(filename))
    target_dir = os.path.join(src_dir, "{}_Label_check".format(Breast_type))
    targets = os.listdir(target_dir)

    if not os.path.exists(dst_dir):
        print("[INFO] Create dir : {}".format(dst_dir))
        os.makedirs(dst_dir)

    fp = open(os.path.join(src_dir, filename), "r")
    row_datas = fp.readlines()
    fp.close()
    row_datas = row_datas[1:]
    for row_data in row_datas:
        row_split = row_data.replace("\n", "").split(",")
        # print(row_split)
        if int(row_split[-1]) == 1:
            index = int(row_split[0]) - 1
            shutil.copyfile(os.path.join(target_dir, targets[index]), os.path.join(dst_dir, targets[index]))


def check_table(row_datas):
    print("[DEBUG] Check row_datas : ")
    for row_data in row_datas:
        print(row_data.replace("\n", ""))


def mark_data():
    # 2020/04/03
    selected_benign = [ 1,  2,  3,  12, 11, 
                        4,  5,  6,  7,  9,
                        8,  10, 13, 14, 15, 
                        16, 17, 18, 24, 25,
                        26, 27, 28, 29 ]
    mark_selected(r"../Matlab/Database/",
                  "Breast_Benign_tb.csv", selected_benign)
    
    selected_malignant = [ 1,  2,  3,  4,  5,
                           6,  7,  8,  9,  10, 
                           11, 12, 13, 14, 15, 
                           16, 17, 18, 25, 26, 
                           27, 28, 29, 30 ]
    mark_selected(r"../Matlab/Database/",
                 "Breast_Malignant_tb.csv", selected_malignant)

    
    
    # 2020/4/17
    selected_benign = [ 33, 34, 41, 42, 44, 45, 48, 49,
                        50, 51, 19, 20, 23, 52, 53, 54,
                        55, 56, 57, 60, 61, 62, 63, 64,
                        65, 66, 67, 68, 68, 69, 70, 71,
                        72 ]
    mark_selected(r"../Matlab/Database/",
                  "Breast_Benign_tb.csv", selected_benign)

    selected_malignant = [ 31, 32, 33, 34, 35, 36, 37, 38, 
                           41, 43, 42, 44, 45, 46, 47, 48,
                           49, 19, 20, 50, 51, 52, 53, 54,
                           55, 56, 57, 23, 24, 58, 59, 60 ]
    mark_selected(r"../Matlab/Database/",
                 "Breast_Malignant_tb.csv", selected_malignant)

    # 2020/10/05
    selected_benign = [ 72,  73,  74,  75,  76,  77,  78,  81,  82,  83,  84,  85,  86,  87,  88,
                        89,  90,  91,  92,  93,  94,  95,  96,  97,  98,  99,  106, 107, 108, 109, 
                        110, 103, 104, 113, 114, 115, 116, 117, 118, 120, 121, 122, 123, 111, 112,
                        126, 127, 128 ]
    mark_selected(r"../Matlab/Database/",
                  "Breast_Benign_tb.csv", selected_benign)

    selected_malignant = [ 60,  61,  62,  63,  64,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  
                           77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  90,  91,  92,  93,
                           94,  95,  96,  97,  98,  101, 102, 103, 104, 105, 88,  89,  106, 107, 108,
                           111, 112, 113  ]
    mark_selected(r"../Matlab/Database/",
                 "Breast_Malignant_tb.csv", selected_malignant)

    # 2021/04/05
    selected_benign = [ 128, 129, 130, 131, 126, 127, 128, 129, 130,
                        131, 132, 133, 134, 135, 136, 137, 138, 139,
                        140, 141, 142, 143, 144, 149, 151, 152, 153,
                        154, 155, 160, 161, 162, 163, 164, 165, 166,
                        167, 168, 169, 170, 171, 172, 173, 174, 175,
                        179, 180, 181, 182, 187, 188, 189, 190, 191 ]
    mark_selected(r"../Matlab/Database/",
                  "Breast_Benign_tb.csv", selected_benign)
    
    selected_malignant = [ 113, 114, 115, 116, 117, 118, 106, 107, 108,
                           111, 112, 113, 114, 115, 116, 117, 118, 119,
                           120, 121, 122, 123, 124, 125, 126, 127, 128,
                           129, 132, 133 ,134, 135, 136, 137, 138, 139,
                           142, 143, 144, 145, 146, 147, 148, 151, 152,
                           153, 154, 155 ,156, 157, 158, 159, 160, 161 ]
    mark_selected(r"../Matlab/Database/",
                 "Breast_Malignant_tb.csv", selected_malignant)

    # 2021/04/11
    selected_benign = [ 192, 193, 194, 195, 196, 199, 200, 201, 202,
                        203, 204, 205, 209, 210, 213, 214, 215, 216,
                        217, 220, 221, 222, 223, 224, 225, 226, 227,
                        228, 229, 230, 231, 234, 235, 237, 238, 239,
                        240, 241, 242, 243, 244, 245, 246, 247, 249,
                        250, 251, 252, 253, 254, 255, 256, 257, 258 ]
    mark_selected(r"../Matlab/Database/",
                  "Breast_Benign_tb.csv", selected_benign)
    
    selected_malignant = [ 162, 163, 164, 165, 166, 167, 168, 169, 170, 
                           171, 172, 173, 174, 175, 176, 177, 182, 183,
                           184, 185, 186, 187, 188, 189, 190, 191, 192,
                           193, 194, 119, 120, 121, 122, 123, 124, 125,
                           126, 127, 128, 129, 132, 133, 134, 135, 136,
                           137, 138, 139, 142, 143, 144, 145, 146, 147 ]
    mark_selected(r"../Matlab/Database/",
                 "Breast_Malignant_tb.csv", selected_malignant)


   
def main():
    mark_data()

    write_labelImg_Name("Benign", r"../Matlab/Database/Benign_label")
    write_labelImg_Name("Malignant", r"../Matlab/Database/Malignant_label")
    
    selectd_label_check("Benign", r"../Matlab/Database/",
                        "Breast_Benign_tb.csv", r"./Labeled/Benign")
    selectd_label_check("Malignant", r"../Matlab/Database/",
                        "Breast_Malignant_tb.csv", r"./Labeled/Malignant")


if __name__ == '__main__':
    main()
