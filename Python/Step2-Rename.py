import shutil
import os

pwd = "D:/DST/逢大專區/108 碩士計畫/SegNet 論文/breath_us_segmentation/"
database = pwd + "Matlab/Database/"
ground_truth_path = pwd + "Matlab/Ground Truth Labeler/"


def img_origin_rename(breath_type, src, dst, rewrite=False):
    if rewrite is True:
        shutil.rmtree(os.path.join(database, breath_type))
        os.makedirs(os.path.join(database, breath_type))

    # List files
    img_list = os.listdir(src)
    img_list.sort()
    # Write tables
    table_name = "Breast_{}_tb.csv".format(breath_type)
    target_fp = os.path.join(database, table_name)
    if os.path.isfile(target_fp) and rewrite is False:
        print("[INFO] Appending data to \"{}\"...".format(target_fp))
        fp = open(target_fp, "r")
        lines = fp.readlines()
        fp.close()
        fp = open(target_fp, "a")
        line_split = lines[-1].split(",")
        last_id = int(line_split[0]) + 1
        for index, fname in enumerate(img_list):
            find = False
            for line in lines:
                line_split = line.split(",")
                if line_split[2].find(fname) != -1:
                    find = True
                    break
            if find is True:
                continue
            else:
                new_fname = "%05d_origin_%s_%s" % (last_id, breath_type, fname)
                fp.write("{},{},{},{},,0\n".format(
                    last_id, breath_type, fname, new_fname))
                last_id += 1
        fp.close()
    else:
        print("[INFO] Create database \"{}\"...".format(target_fp))
        fp = open(target_fp, "w")
        fp.write("id,type,origin filename,new filename,label filename,selected\n")
        for index, fname in enumerate(img_list):
            new_fname = "%05d_origin_%s_%s" % (index+1, breath_type, fname)
            fp.write("{},{},{},{},,0\n".format(
                index+1, breath_type, fname, new_fname))
            shutil.copyfile(os.path.join(src, fname),
                            os.path.join(dst, new_fname))
        fp.close()


def img_label_rename(breath_type, src, dst):
    table_name = "Breast_{}_tb.csv".format(breath_type)
    print("[INFO] Adding labeling image into database \"{}\"...".format(table_name))
    fp = open(os.path.join(database, table_name), "r")
    lines = fp.readlines()
    fp.close()
    fp = open(os.path.join(database, table_name), "w")
    for fname in os.listdir(src):
        num = ''.join([x for x in fname if x.isdigit()])
        num = int(num)
        new_fname = "%05d_%s.png" % (num, breath_type)
        line_split = lines[num].split(",")
        lines[num] = "{},{},{},{},{},{}".format(
            line_split[0], line_split[1], line_split[2], line_split[3], new_fname, line_split[5])
        #print(lines[num].replace("",""))
        shutil.copyfile(os.path.join(src, fname), os.path.join(dst, new_fname))
    print(lines)
    fp.writelines(lines)
    fp.close()


def rename_all():
    cutresult_dir = "./Dataset_cut/Cut Result"
    img_origin_rename('Benign', os.path.join(cutresult_dir, 'Benign_All'),
                      database + 'Benign', True)
    img_origin_rename('Malignant', os.path.join(cutresult_dir, 'Malignant_All'),
                      database + 'Malignant', True)


def rename_gtl():
    img_label_rename('Benign', ground_truth_path + 'Benign/PixelLabelData/',
                     database + 'Benign_label/')
    img_label_rename('Malignant', ground_truth_path + 'Malignant/PixelLabelData',
                     database + 'Malignant_labeled/')


if __name__ == '__main__':
    rename_all()
    # rename_gtl()
