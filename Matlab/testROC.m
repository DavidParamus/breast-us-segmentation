%% Reset 
clear;
clc;

%% 載入網路
%load net
network_dir = './networks/';
fprintf("[INFO] Loading networking form '%s'...\n", network_dir);
temp_struct = load(fullfile(network_dir,'network_deeplabv3plus_inceptionresnetv2_10folds_4.mat'));
network = temp_struct.netTransfer;

dsm_table = table;  % Save networks' DataSetMetrics 
fprintf("[INFO] Loading completed! Start to evaluate network...\n");

%% EvaluateDeeplab v3 plus
% Deeplabv3plus InceptionResNet v2
netName = 'deeplabNet_inceptionresnetv2';

fprintf("[INFO] Evaluating network: %s...\n",netName);
run_date = "test_ROC_curve";

% 建立Label
classes = [
    "Shadowing"
    %"Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
];
classNames = strrep(classes, "_","\_");
cmap = breathSegColorMap();
labelIDs = breathSegLabels();

% 開啟測試檔
imgDir =fullfile(pwd,'Dataset\Dataset-20210430\Test_image\');
imgDirS = dir(fullfile(imgDir, '*.png'));
imgDirN = natsortfiles({imgDirS.name});
imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
imdsTest = imageDatastore(string(imgDirF));

labelDir = fullfile(pwd,'Dataset\Dataset-20210430\Test_label\');
labelDirS = dir(fullfile(labelDir, '*.png'));
labelDirN = natsortfiles({labelDirS.name});
labelDirF = cellfun(@(n)fullfile(labelDir,n),labelDirN,'uni',0);

pxdsTest = pixelLabelDatastore(labelDirF,classes,labelIDs);
tempdir = fullfile(pwd,'Run_Result', run_date);
if ~exist(tempdir, 'dir')
    mkdir(tempdir)
end

% Write result file 
writeDir = fullfile(pwd,'\Run_Result\', run_date, '\Evaluate_result\');
if ~exist(writeDir, 'dir')
    mkdir(writeDir)
end

if ~exist(fullfile(writeDir, netName), 'dir')
    mkdir(fullfile(writeDir, netName));
end

if ~exist(fullfile(writeDir, netName, 'ROC_Curve'), 'dir')
    mkdir(fullfile(writeDir, netName, 'ROC_Curve'));
end

pxdsResults = semanticseg(imdsTest,network, ...
        'MiniBatchSize',5, ...
        'WriteLocation', tempdir,...
        'Verbose',false);

metrics = evaluateSemanticSegmentation(pxdsResults,pxdsTest,'Verbose',false);
filename = sprintf('BSS-%s-ClassMetrics.csv', netName);
writetable(metrics.ClassMetrics, fullfile(writeDir, netName, filename));
dsm = metrics.DataSetMetrics;

% Confusion Matrix
fig = figure(1);
normConfMatData = metrics.NormalizedConfusionMatrix.Variables;
h = heatmap(classNames,classNames,100*normConfMatData);
h.XLabel = 'Predicted Class';
h.YLabel = 'True Class';
h.Title = 'Normalized Confusion Matrix (%)';
filename = sprintf('BSS-%s-NormalizedConfusionMetrics.png', netName);
saveas(fig,fullfile(writeDir, netName, filename));
close(fig);

fig = figure(2);
confMatData = metrics.ConfusionMatrix.Variables;
h = heatmap(classNames,classNames,confMatData);
h.XLabel = 'Predicted Class';
h.YLabel = 'True Class';
h.Title = 'Confusion Matrix (Pixelwise)';
filename = sprintf('BSS-%s-ConfusionMetrics.png', netName);
saveas(fig,fullfile(writeDir, netName, filename));
close(fig);

%% ROC Curves
auclogs = table();
fileNum = length(imdsTest.Files);
for k = 1:fileNum
    % Log & Info usage
    [~, targetName, targetExt] = fileparts(imdsTest.Files(k));
    targetFile =  [targetName, targetExt];
    
    % Read file and ground truth
    img = readimage(imdsTest, k);
    gt = readimage(pxdsTest, k);
    [pred, score, allScore] = semanticseg(img, network);
    % Convert to 1D array
    gt1D = reshape(gt,[1, size(gt,1)*size(gt,2)] );
    allScore1D = reshape(allScore,[ size(allScore,1) * size(allScore,2), size(allScore,3)] );
    
    % Check exception : All same
    if all(gt1D == classes(end))
        fprintf("[INFO] FileNum:%d: %s : All label is same, pass.\n", k, targetFile);
        continue;
    end
    
    % Plot ROC
    legendClass=[];
    isEception = false;
    for i = 1:size(classes)
        if ismember(classes(i) ,gt) && i ~= 7
            try
                [x,y,~, auc] = perfcurve(gt1D,allScore1D(:,i),classes(i));
                legendStr = sprintf("%s (AUC: %5.2f%%)", classNames(i), auc*100);
                legendClass = [legendClass;legendStr];
                plot(x,y, 'Color', cmap(i,:));
                hold on;
                auclog = table(k,{targetName}, {classes(i)},auc,'VariableNames',{'Index','Filename', 'Class','AUC'});
                auclogs = [auclogs; auclog];
            catch ME
                warning('[ERROR] Exception happend! FIle: %s',targetFile);
                isEception = true;
                rethrow(ME)
            end
        end
    end
    
    if ~isEception
        legend(legendClass);
        xlabel('False positive rate');
        ylabel('True positive rate');
        hold off;
        filename = sprintf("ROC_Curve_%s_%d_%d_%s", netName, 4, k, targetFile);
        destination = fullfile(writeDir, netName, 'ROC_Curve', filename);
        saveas(gcf,destination);
        close(gcf);
    end
end

tablename = sprintf('%s_ROCcurveAuc.csv', netName);
destination = fullfile(writeDir, netName, 'ROC_Curve', tablename);
writetable(auclogs, destination);

dsmNetwork = table({netName}, 'VariableNames',{'Network'});  
dsm_table = [dsm_table; dsm];


%% Close file
writeDir = fullfile(pwd,'\Run_Result\', run_date, '\Evaluate_result\');
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.csv'));

fprintf("[INFO] Evaluation completed!\n");