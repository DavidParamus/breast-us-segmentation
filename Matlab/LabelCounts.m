%% Reset 
clear;
clc;
close all;
diary off;
%% Load Class
% Create Label
classes = [
    "Shadowing"
    %"Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
];
classNames = strrep(classes, "_","\_");
cmap = breathSegColorMap();
labelIDs = breathSegLabels();

%% Origin
fprintf("[INFO] Counting Original Image labels\n");
% Load Dataset
imgDir =fullfile(pwd,'Dataset/Train_image/');
imgDirS = dir(fullfile(imgDir, '*.png'));
imgDirN = natsortfiles({imgDirS.name});
imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
imds = imageDatastore(string(imgDirF));

labelDir = fullfile(pwd,'Dataset/Train_label/');
labelDirS = dir(fullfile(labelDir, '*.png'));
labelDirN = natsortfiles({labelDirS.name});
labelDirF = cellfun(@(n)fullfile(labelDir,n),labelDirN,'uni',0);
pxds = pixelLabelDatastore(labelDirF,classes,labelIDs);

num_folds = 10;

fileNum = length(imds.Files);

fprintf("[INFO] Counting pixels of each label...\n");
pixel_counts = countEachLabel(pxds);
fprintf("[INFO] Counting images of each label...\n");
image_counts = zeros(size(classes));
for i = 1:fileNum
    img = readimage(imds, i); 
    gt = readimage(pxds, i);
    for k = 1:length(classes)
        if ismember(classes(k), gt)
            image_counts(k) = image_counts(k) + 1;
        end
    end
end
% write table
count_table = [pixel_counts, table(image_counts, 'VariableNames',{'ImageCount'})];

tablename= "GroundTruthLabelCounts.xlsx";
writetable(count_table, fullfile(pwd,"Dataset", tablename), 'Sheet', "origin");

%% Data Augmentation
fprintf("[INFO] Counting Augmented Image labels\n");
% Load Dataset
imgDir =fullfile(pwd,'Dataset/Train_image_Aug/');
imgDirS = dir(fullfile(imgDir, '*.png'));
imgDirN = natsortfiles({imgDirS.name});
imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
imds = imageDatastore(string(imgDirF));

labelDir = fullfile(pwd,'Dataset/Train_label_Aug/');
labelDirS = dir(fullfile(labelDir, '*.png'));
labelDirN = natsortfiles({labelDirS.name});
labelDirF = cellfun(@(n)fullfile(labelDir,n),labelDirN,'uni',0);
pxds = pixelLabelDatastore(labelDirF,classes,labelIDs);

num_folds = 10;

fileNum = length(imds.Files);

fprintf("[INFO] Counting pixels of each label...\n");
pixel_counts = countEachLabel(pxds);
fprintf("[INFO] Counting images of each label...\n");
image_counts = zeros(size(classes));
for i = 1:fileNum
    img = readimage(imds, i); 
    gt = readimage(pxds, i);
    for k = 1:length(classes)
        if ismember(classes(k), gt)
            image_counts(k) = image_counts(k) + 1;
        end
    end
end
% write table
count_table = [pixel_counts, table(image_counts, 'VariableNames',{'ImageCount'})];

tablename= "GroundTruthLabelCounts.xlsx";
writetable(count_table, fullfile(pwd,"Dataset", tablename), 'Sheet', "augmented");