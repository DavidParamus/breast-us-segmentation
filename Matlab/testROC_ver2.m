%% Reset 
clear;
clc;
close all;

%% 載入網路
%load net
network_dir = './networks/';
fprintf("[INFO] Loading networking form '%s'...\n", network_dir);
temp_struct = load(fullfile(network_dir,'network_fcn_32s_10folds_10.mat'));
network = temp_struct.netTransfer;

dsm_table = table;  % Save networks' DataSetMetrics 
fprintf("[INFO] Loading completed! Start to evaluate network...\n");

%% EvaluateDeeplab v3 plus
% Deeplabv3plus InceptionResNet v2
netName = 'network_fcn_32s_10folds_10';

fprintf("[INFO] Evaluating network: %s...\n",netName);
run_date = "test_ROC_curve";

% 建立Label
classes = [
    "Shadowing"
    %"Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
];
classNames = strrep(classes, "_","\_");
cmap = breathSegColorMap();
labelIDs = breathSegLabels();

% 開啟測試檔
imgDir =fullfile(pwd,'Dataset\Dataset-20210430\Test_image\');
imgDirS = dir(fullfile(imgDir, '*.png'));
imgDirN = natsortfiles({imgDirS.name});
imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
imdsTest = imageDatastore(string(imgDirF));

labelDir = fullfile(pwd,'Dataset\Dataset-20210430\Test_label\');
labelDirS = dir(fullfile(labelDir, '*.png'));
labelDirN = natsortfiles({labelDirS.name});
labelDirF = cellfun(@(n)fullfile(labelDir,n),labelDirN,'uni',0);

pxdsTest = pixelLabelDatastore(labelDirF,classes,labelIDs);
tempdir = fullfile(pwd,'Run_Result', run_date);
if ~exist(tempdir, 'dir')
    mkdir(tempdir)
end

% Write result file 
writeDir = fullfile(pwd,'\Run_Result\', run_date, '\Evaluate_result\');
if ~exist(writeDir, 'dir')
    mkdir(writeDir)
end

if ~exist(fullfile(writeDir, netName), 'dir')
    mkdir(fullfile(writeDir, netName));
end

pxdsResults = semanticseg(imdsTest,network, ...
        'MiniBatchSize',5, ...
        'WriteLocation', tempdir,...
        'Verbose',false);

metrics = evaluateSemanticSegmentation(pxdsResults,pxdsTest,'Verbose',false);
filename = sprintf('BSS-%s-ClassMetrics.csv', netName);
writetable(metrics.ClassMetrics, fullfile(writeDir, netName, filename));
dsm = metrics.DataSetMetrics;

% Confusion Matrix
fig = figure(1);
normConfMatData = metrics.NormalizedConfusionMatrix.Variables;
h = heatmap(classNames,classNames,100*normConfMatData);
h.XLabel = 'Predicted Class';
h.YLabel = 'True Class';
h.Title = 'Normalized Confusion Matrix (%)';
filename = sprintf('BSS-%s-NormalizedConfusionMetrics.png', netName);
saveas(fig,fullfile(writeDir, netName, filename));
close(fig);

fig = figure(2);
confMatData = metrics.ConfusionMatrix.Variables;
h = heatmap(classNames,classNames,confMatData);
h.XLabel = 'Predicted Class';
h.YLabel = 'True Class';
h.Title = 'Confusion Matrix (Pixelwise)';
filename = sprintf('BSS-%s-ConfusionMetrics.png', netName);
saveas(fig,fullfile(writeDir, netName, filename));
close(fig);

dsmNetwork = table({netName}, 'VariableNames',{'Network'});  
dsm_table = [dsm_table; dsm];

evaluateCriterion = 'WeightedIoU';
if ~exist(fullfile(writeDir, netName, 'ROC_Curve'), 'dir')
    mkdir(fullfile(writeDir, netName, 'ROC_Curve'));
end

% ROC Curves
rocTable = table();
imgMetrix = metrics.ImageMetrics;
rocThreshold = 0.5;
fprintf("[INFO] Evaluation Criterions: %s ; Threshold : %5.2f%%.\n", evaluateCriterion, rocThreshold*100);
rocLabels = [
    "Correct"
    "Wrong"
];

for k = 1:size(imgMetrix,1)
    % Log & Info usage
    [~, targetName, targetExt] = fileparts(imdsTest.Files(k));
    targetFile =  [targetName, targetExt];

    % if Weighted IoU > Threshold ? 'Correct' : 'Wrong'
    target = imgMetrix{k, evaluateCriterion};   
    if target > rocThreshold
        rocLabel = rocLabels(1);
        globalAccuracy = imgMetrix{k, 'GlobalAccuracy'};
        meanAccuracy = imgMetrix{k, 'MeanAccuracy'};
        meanIoU = imgMetrix{k, 'MeanIoU'};
        weightedIoU =imgMetrix{k, 'WeightedIoU'};
        meanBFScore = imgMetrix{k, 'MeanBFScore'};
         else
        rocLabel = rocLabels(2);
        globalAccuracy = 1 - imgMetrix{k, 'GlobalAccuracy'};
        meanAccuracy = 1 -imgMetrix{k, 'MeanAccuracy'};
        meanIoU = 1 - imgMetrix{k, 'MeanIoU'};
        weightedIoU = 1 - imgMetrix{k, 'WeightedIoU'};
        meanBFScore = 1 - imgMetrix{k, 'MeanBFScore'};
    end

    % Write to ROC used table
    rocRow = table(string(rocLabel), globalAccuracy, meanAccuracy, meanIoU, weightedIoU, meanBFScore, ...
                      'VariableNames',{'Label', 'GlobalAccuracy', 'MeanAccuracy', 'MeanIoU', 'WeightedIoU', 'MeanBFScore'});
    rocTable = [rocTable; rocRow];

end

figure(3);
[x,y,~, auc] = perfcurve(rocTable{:,'Label'}, rocTable{:,evaluateCriterion},rocLabels(1));
plot(x,y, '-b');
title(sprintf("ROC Curve - %s (AUC: %5.2f%% / Threshold: %5.2f)", evaluateCriterion ,auc*100, rocThreshold*100 )); 
xlabel('False positive rate');
ylabel('True positive rate');
filename = sprintf("ROC_Curve_%s_%d_%s.png", netName, k, evaluateCriterion);
destination = fullfile(writeDir,netName, 'ROC_Curve', filename);
saveas(gcf,destination);
close(gcf);

%% Close file
writeDir = fullfile(pwd,'\Run_Result\', run_date, '\Evaluate_result\');
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.csv'));

fprintf("[INFO] Evaluation completed!\n");