%% Reset 
clear;
clc;
close all;
diary off;


%% 載入網路
%load net
%network_dir = './networks/';
network_dir = './Run_Result/2021-06-15/';

fprintf("[INFO] Loading networking form '%s'...\n", network_dir);

% Write Dir
today = datetime('today');
run_date = datestr(today, 'yyyy-mm-dd');
writeDir = fullfile(pwd,'/Run_Result/', run_date, '/Evaluate_result/');

% 建立Label
classes = [
    "Shadowing"
    %"Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
];
classNames = strrep(classes, "_","\_");
cmap = breathSegColorMap();
labelIDs = breathSegLabels();

% Load Dataset
imgDir =fullfile(pwd,'Dataset/Train_image_Aug/');
imgDirS = dir(fullfile(imgDir, '*.png'));
imgDirN = natsortfiles({imgDirS.name});
imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
imds = imageDatastore(string(imgDirF));

labelDir = fullfile(pwd,'Dataset/Train_label_Aug/');
labelDirS = dir(fullfile(labelDir, '*.png'));
labelDirN = natsortfiles({labelDirS.name});
labelDirF = cellfun(@(n)fullfile(labelDir,n),labelDirN,'uni',0);
pxds = pixelLabelDatastore(labelDirF,classes,labelIDs);

num_folds = 10;


%% EvaluateDeeplab v3 plus - k-fold
% Deeplabv3plus InceptionResNet v2
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_deeplabv3plus_inceptionresnetv2';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName, 'networks', netName_fold));
    deeplabNet_inceptionresnetv2 = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, deeplabNet_inceptionresnetv2, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);

% Deeplabv3plus mobilev2
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_deeplabv3plus_mobilenetv2';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName, 'networks', netName_fold));
    deeplabNet_mobilev2 = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable]= evaluateNetwork(i, num_folds, imds, pxds, deeplabNet_mobilev2, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);

% Deeplabv3plus xception
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_deeplabv3plus_xception';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName,  'networks', netName_fold));
    deeplabNet_xception = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, deeplabNet_xception, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end 
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);

% Deeplabv3plus resnet18
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_deeplabv3plus_resnet18';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName,  'networks', netName_fold));
    deeplabNet_resnet18 = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, deeplabNet_resnet18, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);

% Deeplabv3plus resnet50
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_deeplabv3plus_resnet50';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName,  'networks', netName_fold));
    deeplabNet_resnet50 = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, deeplabNet_resnet50, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end 
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);


%% Evaluate Segnet
% vgg16
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_segnet_vgg16';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName,  'networks', netName_fold));
    segnet_vgg16 = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, segnet_vgg16, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end 
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);

% vgg19
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_segnet_vgg19';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName,  'networks', netName_fold));
    segnet_vgg19 = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, segnet_vgg19, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end 
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);


%% Evluate U-Net
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_unet_depth4';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName,  'networks', netName_fold));
    unet = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, unet, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];  
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end 
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);


%% Evluate FCN
rocTables = table();
dsm_table = table();  
positive_dsm_table = table();
for i = 1:num_folds
    netName = 'network_fcn_32s';
    netName_fold = sprintf('%s_10folds_%d.mat', netName, i);
    temp_struct = load(fullfile(network_dir, netName,  'networks', netName_fold));
    fcn = temp_struct.netTransfer;
    [dsm, posDsmRow, rocTable] = evaluateNetwork(i, num_folds, imds, pxds, fcn, netName, classes, rocTables);
    rocTables = [rocTables; rocTable];
    dsm_table = [dsm_table; dsm];
    positive_dsm_table = [positive_dsm_table; posDsmRow];
end 
writetable(dsm_table, fullfile(writeDir, 'DataSetMetrics.xlsx'), 'Sheet', netName);
writetable(positive_dsm_table, fullfile(writeDir, 'PositiveDataSetMetrics.xlsx'), 'Sheet', netName);


%% End of Evaluation
fprintf("[INFO] Evaluation completed!\n");

%% Function: EvaluateNetwork
function [dsm, posDsmRow, rocTable]=evaluateNetwork(fold_idx, num_folds, imds, pxds, network, netName, classes, rocTables)

    [~,netNOnly,~] = fileparts(netName); 
    classNames = strrep(classes, "_","\_");
    cmap = breathSegColorMap();

    warning('off','MATLAB:xlswrite:AddSheet'); % Optional: Disable writting table warning

    fprintf("[INFO] Evaluating network: %s %dfold_%d...\n",netName, num_folds, fold_idx);
    today = datetime('today');
    run_date = datestr(today, 'yyyy-mm-dd');
    
    % Write result file 
    writeDir = fullfile(pwd,'/Run_Result/', run_date, '/Evaluate_result/');
    if ~exist(fullfile(writeDir, netName), 'dir')
        mkdir(fullfile(writeDir, netName));
    end
    
    if ~exist(fullfile(writeDir, netName, 'Class Metrics'), 'dir')
        mkdir(fullfile(writeDir, netName, 'Class Metrics'));
    end
    
    if ~exist(fullfile(writeDir, netName, 'Normalized Confusion Metrics'), 'dir')
        mkdir(fullfile(writeDir, netName, 'Normalized Confusion Metrics'));
    end
    
    if ~exist(fullfile(writeDir, netName, 'Pixelwise Confusion Metrics'), 'dir')
        mkdir(fullfile(writeDir, netName, 'Pixelwise Confusion Metrics'));
    end
    
    if ~exist(fullfile(writeDir, netName, 'ROC Curve'), 'dir')
        mkdir(fullfile(writeDir, netName, 'ROC Curve'));
    end

        
    % Number of Images
    num_images=length(imds.Files);
    
    % Test Indices for current fold
    test_idx=fold_idx:num_folds:num_images;
    
    % Test cases for current fold
    imdsTest = subset(imds,test_idx);
    pxdsTest = subset(pxds,test_idx);
    
    tic;
    pxdsResults = semanticseg(imdsTest,network, ...
        'MiniBatchSize',5, ...
        'WriteLocation', tempdir,...
        'Verbose',true);
    runTime = toc;
    fprintf("[INFO] Testset execution time cost: %5.2f sec.\n", runTime);
    
    metrics = evaluateSemanticSegmentation(pxdsResults,pxdsTest,'Verbose',true);
    tablename = sprintf('ClassMetrics-%s.xlsx', netName);
    writetable(metrics.ClassMetrics, fullfile(writeDir, netName, 'Class Metrics', tablename), 'Sheet',fold_idx);
    
    networkTbl = table({netName}, 'VariableNames',{'Network'});
    runTime_table = table(runTime,  'VariableNames', {'Run Time'});
    dataSetMetrics = metrics.DataSetMetrics;
    dsm = [networkTbl, dataSetMetrics, runTime_table];
    
    % Confusion Matrix
    fig = figure(1);
    normConfMatData = metrics.NormalizedConfusionMatrix.Variables;
    h = heatmap(classNames,classNames,100*normConfMatData);
    h.XLabel = 'Predicted Class';
    h.YLabel = 'True Class';
    h.Title = 'Normalized Confusion Matrix (%)';
    filename = sprintf('NormalizedConfusionMetrics-%s-%dfold_%d.png', netName, num_folds, fold_idx);
    saveas(fig,fullfile(writeDir, netName, 'Normalized Confusion Metrics', filename));
    tablename= sprintf('NormalizedConfusionMetrics-%s.xlsx',netName);
    writetable(metrics.NormalizedConfusionMatrix, fullfile(writeDir, netName,  'Normalized Confusion Metrics', tablename), 'Sheet', fold_idx);
    close(fig);
    
    fig = figure(2);
    confMatData = metrics.ConfusionMatrix.Variables;
    h = heatmap(classNames,classNames,confMatData);
    h.XLabel = 'Predicted Class';
    h.YLabel = 'True Class';
    h.Title = 'Confusion Matrix (Pixelwise)';
    filename = sprintf('PixelwiseConfusionMetrics-%s-%dfold_%d.png', netName, num_folds, fold_idx);
    saveas(fig,fullfile(writeDir, netName, 'Pixelwise Confusion Metrics', filename));
    close(fig);
    tablename= sprintf('PixelwiseConfusionMetrics-%s.xlsx',netName);
    writetable(metrics.ConfusionMatrix, fullfile(writeDir, netName, 'Pixelwise Confusion Metrics', tablename), 'Sheet', fold_idx);
    
    % ROC Curves
    rocTable = table();
    positiveTable = table();
    imgMetrix = metrics.ImageMetrics;
    rocThreshold = 0.75;
    rocLabels = [
        "Correct"
        "Wrong"
    ];
    fileNum = length(imdsTest.Files);
    for k = 1:fileNum
        fprintf("    [INFO] Progress iamges: %d / %d\n", k, fileNum);
        % Log & Info usage
        [~, targetName, targetExt] = fileparts(imdsTest.Files(k));
        targetFile =  [targetName, targetExt];

        % Read file and ground truth
        img = readimage(imdsTest, k);
        gt = readimage(pxdsTest, k);
        
        % Predicated and calculate IoU
        [pred, score, allScore] = semanticseg(img, network);
        positive_row = evaluatepositiveSemanticSegmentation(pred, gt, classes);
        positiveTable = [positiveTable;positive_row];
        positive_iou = positive_row{:,"WeightedpositiveIoU"};
        positive_weighted_iou = positive_row{:,"WeightedpositiveIoU"};
        if isnan(positive_iou)
            positive_iou = 0;
        end
        if isnan(positive_weighted_iou)
            positive_weighted_iou = 0;
        end
        
        % if Weighted IoU > Threshold ? 'Correct' : 'Wrong'
        target = positive_weighted_iou;
        if target > rocThreshold
            rocLabel = rocLabels(1);
            globalAccuracy = imgMetrix{k, 'GlobalAccuracy'};
            meanAccuracy = imgMetrix{k, 'MeanAccuracy'};
            meanIoU = imgMetrix{k, 'MeanIoU'};
            weightedIoU =imgMetrix{k, 'WeightedIoU'};
            meanBFScore = imgMetrix{k, 'MeanBFScore'};
        else
            rocLabel = rocLabels(2);
            positive_iou = 1 - positive_iou;
            positive_weighted_iou = 1 - positive_weighted_iou;
            globalAccuracy = 1 - imgMetrix{k, 'GlobalAccuracy'};
            meanAccuracy = 1 -imgMetrix{k, 'MeanAccuracy'};
            meanIoU = 1 - imgMetrix{k, 'MeanIoU'};
            weightedIoU = 1 - imgMetrix{k, 'WeightedIoU'};
            meanBFScore = 1 - imgMetrix{k, 'MeanBFScore'};
        end
        
        %fprintf('[DEBUG] Progress: %d/%d; positive_weighted_iou:  %.6f; %s\n', k, fileNum, positive_weighted_iou, rocLabel);
        % Write to ROC used table
        rocRow = table(string(rocLabel), globalAccuracy, meanAccuracy, meanIoU, weightedIoU, meanBFScore, positive_iou, positive_weighted_iou, ...
                             'VariableNames',{'Label', 'GlobalAccuracy', 'MeanAccuracy', 'MeanIoU', 'WeightedIoU', 'MeanBFScore', 'positiveIoU', 'WeightedpositiveIoU'});
        rocTable = [rocTable; rocRow];
    end
    % Write positive Data set matrix table
    tableName= sprintf('PositiveDataSetMetrics-%s.xlsx',netName);
    sheetNames = sprintf("%dfold_%d", num_folds, fold_idx);
    writetable(positiveTable, fullfile(writeDir, netName, tableName), 'Sheet', sheetNames);
    
    posDsmRow = table(mean(positiveTable(:,1)), mean(positiveTable(:,2)), mean(positiveTable(:,3)), mean(positiveTable(:,4)), mean(positiveTable(:,5)), ...
                      'VariableNames',{'PositiveGlobalAccuracy', 'PositiveMeanAccuracy','PositiveIoU', 'WeightedpositiveIoU', 'PositiveMeanBFScore'});
    
    %fprintf("[DEBUG] Correct amount: %d\n", sum(rocTable{:,'Label'}=="Correct"));
    evaluateCriterion = 'WeightedpositiveIoU';
    if sum(rocTable{:,'Label'}=="Correct")~=0
        figure(3);
        [x,y,~, auc] = perfcurve(rocTable{:,'Label'}, rocTable{:,evaluateCriterion},rocLabels(1));
        plot(x,y, '-b');
        title(sprintf("ROC Curve by %s (AUC: %5.2f%% / Threshold: %5.2f%%)", evaluateCriterion, auc*100, rocThreshold*100)); 
        xlabel('False positive rate');
        ylabel('True positive rate');
        filename = sprintf("ROC_Curve_%s_%dfold_%d_%s.png", netName, num_folds, fold_idx, evaluateCriterion);
        destination = fullfile(writeDir,netName, 'ROC Curve', filename);
        saveas(gcf,destination);
        close(gcf);
    else
        fprintf("[INFO] No correct label found, skip ROC curve. \n");
    end
        
    if num_folds == fold_idx && sum(rocTables{:,'Label'}=="Correct")~=0
        figure(4);
        [x,y,~, auc] = perfcurve(rocTables{:,'Label'}, rocTables{:,evaluateCriterion},rocLabels(1));
        plot(x,y, '-b');
        title(sprintf("ROC Curve by %s (AUC: %5.2f%% / Threshold: %5.2f%%)", evaluateCriterion, auc*100, rocThreshold*100)); 
        xlabel('False positive rate');
        ylabel('True positive rate');
        filename = sprintf("ROC_Curve_%s_%s_%s.png", netName, 'Overall', evaluateCriterion);
        destination = fullfile(writeDir,netName, filename);
        saveas(gcf,destination);
        close(gcf);
    else 
        fprintf("[INFO] No correct label found, skip ROC curve. \n");
    end
end


%% Count each classes' pixels of each images
function counts = count_each_label(gt, classes)
    counts = zeros(7,1);
    for i = 1:length(classes)
        count = sum(gt(:)==classes(i));
        counts(i) = count;
    end
end


%% Evaluate Positive Semantic Segmentation (Remove "Background")
function positive_row = evaluatepositiveSemanticSegmentation(pred, gt, classes)
    
    % Positive Global Accuracy 
    positiveGlobalAccuracy = sum(gt == pred, 'all') / (size(gt, 1) * size(gt, 2));
    
    % Mean Accuracy 
    positiveMeanAccuracy = 0;
    cmOrder = [char(classes);'<undefined>     '];
    cm = confusionmat(char(gt), char(pred), 'Order', cmOrder);
    %confusionchart(cm); % Debug
    accuracies = zeros(size(classes));
    for i = 1:size(classes,1)
        if cm(i,i) == 0
            accuracies(i,1) = 0;
            continue;
        else
            accuracies(i,1) = cm(i,i) / sum(cm(7,:));
        end
    end    
    positive_meanAccuracy = mean(accuracies(accuracies(1:end-1) > 0));
    
    % Positive IoU 
    iou = jaccard(pred, gt);
    positive_iou = mean(iou(~isnan(iou(1:end-1))));
    
    % Positive Weighted IoU 
    counts = count_each_label(gt, classes);
    normalized_counts = counts(:)/sum(counts(:));
    rev_counts = 1 + (1 - normalized_counts);
    weighted_iou = iou + times(iou,rev_counts);
    positive_weighted_iou = mean(weighted_iou(~isnan(weighted_iou(1:end-1))));
    
    % Positive BF score
    meanBFScore = bfscore(pred,gt);
    positiveMeanBFScore = mean(meanBFScore(~isnan(meanBFScore(1:end-1))));
    positive_row = table(positiveGlobalAccuracy, positive_meanAccuracy, positive_iou, positive_weighted_iou, positiveMeanBFScore, ...
                             'VariableNames',{'PositiveGlobalAccuracy', 'PositiveMeanAccuracy','PositiveIoU', 'WeightedpositiveIoU', 'PositiveMeanBFScore'});
end