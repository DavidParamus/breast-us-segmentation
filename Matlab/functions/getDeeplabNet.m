function lgraph = getDeeplabNet(imageSize, numClasses, deeplabnet_name, backbone_type)

% If ResNet is not exist. Create one
if ~exist(deeplabnet_name,'file')
    fprintf('Creating DeepLabv3+Net with %s...\n', backbone_type);
    lgraph = deeplabv3plusLayers(imageSize, numClasses, backbone_type);
    % figure('Name', deeplabnet_name);
    % plot(lgraph);
else 
    str = fprintf('Opening DeepLabv3+Net: %s...\n', deeplabnet_name)
    disp(str);
    
    %load net
    deeplabnet = load(deeplabnet_name,'net');
    net = deeplabnet.net;
    lgraph = layerGraph(net);

end

end