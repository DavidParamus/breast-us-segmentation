function lgraph = getSegNet(imageSize, numClasses, segnet_name, network_type)

% If SegNet is not exist. Create one
if ~exist(segnet_name,'file')
    disp(['Creating SegNet with ', network_type, ' ...']);
    lgraph = segnetLayers(imageSize,numClasses, network_type);
    % figure('Name', segnet_name);
    % plot(lgraph);
else 
    str = fprintf('Opening SegNet: %s...', segnet_name);
    disp(str);
    
    %load net
    segnet = load(segnet_name,'net');
    net = segnet.net;
    lgraph = layerGraph(net);

end

end
