function lgraph = getUNet(imageSize, numClasses, unet_name)

% If ResNet is not exist. Create one
if ~exist(unet_name,'file')
    encoderDepth = 3;
    str = fprintf('Creating U-Net with encoder Depth: %d...', encoderDepth);
    disp(str);
    lgraph = unetLayers(imageSize, numClasses, 'EncoderDepth',encoderDepth);
    % figure('Name', unet_name);
    % plot(lgraph);
else 
    str = fprintf('Opening U-Net: %s...', unet_name);
    disp(str);
    
    %load net
    unet = load(unet_name,'net');
    net = unet.net;
    lgraph = layerGraph(net);

end

end