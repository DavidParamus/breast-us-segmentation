function lgraph = getFCN(imageSize, numClasses, fcnnet_name, fcn_type)

% If SegNet is not exist. Create one
if ~exist(fcnnet_name,'file')
    fprintf("Creating FCN with type: %s...\n", fcn_type);
    imageSize = imageSize(1:2);
    lgraph = fcnLayers(imageSize, numClasses, 'Type',fcn_type);
    % figure('Name', segnet_name);
    % plot(lgraph);
else 
    str = fprintf('Opening FCN : %s...', fcnnet_name);
    disp(str);
    
    %load net
    fcn = load(fcnnet_name,'net');
    net = fcn.net;
    lgraph = layerGraph(net);

end

end
