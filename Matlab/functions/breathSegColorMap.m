function cmap = breathSegColorMap()
% Define the colormap used by CamVid dataset.

cmap = [
    0   115 194 % Shadowing : Blue
%    220 82  20  % Thick Echogenic Halo : Orange
    240 175 30  % Taller Than Wide : Yellow
    125 45  145 % Mircolobulation : Purple
    120 175 45  % Hypoechogenicity :Green
    80  190 240 % Duct Extention :Light Blue
    158 24  48  % Angular Margins: Burgundy
    0   0   0   % Background : black
];

% Normalize between [0 1].
cmap = cmap ./ 255;
end