%% Clear workspace and console
clc;
clear;
diary off;

%% 載入資料集
% 圖片輸入大小
inputSize = [360, 360, 3]; %[height, weight, channel]

% 建立 Class (label)
classes = [
    "Shadowing"
%    "Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
]; 
numClasses = length(classes);

labelIDs = breathSegLabels();

% 載入訓練集圖片 
imgDir =fullfile(pwd,'Dataset/Train_image_Aug/');
imgDirS = dir(fullfile(imgDir, '*.png'));
imgDirN = natsortfiles({imgDirS.name});
imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
imds = imageDatastore(string(imgDirF));

% 載入訓練集 Ground Truth 
labelDir = fullfile(pwd,'Dataset/Train_label_Aug/');
labelDirS = dir(fullfile(labelDir, '*.png'));
labelDirN = natsortfiles({labelDirS.name});
labelDirF = cellfun(@(n)fullfile(labelDir,n),labelDirN,'uni',0);
pxds = pixelLabelDatastore(labelDirF,classes,labelIDs);

% 載入驗證集圖片
valDir =fullfile(pwd,'Dataset/Validation_image/');
valDirS = dir(fullfile(valDir, '*.png'));
valDirN = natsortfiles({valDirS.name});
valDirF = cellfun(@(n)fullfile(valDir,n),valDirN,'uni',0);
valImds = imageDatastore(string(valDirF));

% 載入驗證集 Ground Truth 
vallabelDir = fullfile(pwd,'Dataset/Validation_label/');
vallabelDirS = dir(fullfile(vallabelDir, '*.png'));
vallabelDirN = natsortfiles({vallabelDirS.name});
vallabelDirF = cellfun(@(n)fullfile(vallabelDir,n),vallabelDirN,'uni',0);
valPxds = pixelLabelDatastore(vallabelDirF,classes,labelIDs);

pximds = pixelLabelImageDatastore(imds,pxds);
valPximds = pixelLabelImageDatastore(valImds, valPxds);

today = datetime('today');
run_date = datestr(today, 'yyyy-mm-dd');


%% 訓練網路
%------------------------------
% 訓練 DeeplabNet v3 - 5 types
%------------------------------
%{
deeplabNet_name = 'network_deeplabv3plus_mobilenetv2.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "mobilenetv2");
deeplabnet = trainNetWork(deeplabNet_name, deeplabnet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(deeplabnet);

deeplabNet_name = 'network_deeplabv3plus_inceptionresnetv2.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "inceptionresnetv2");
deeplabnet = trainNetWork(deeplabNet_name, deeplabnet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(deeplabnet);
%}
deeplabNet_name = 'network_deeplabv3plus_xception.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "xception");
deeplabnet = trainNetWork(deeplabNet_name, deeplabnet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(deeplabnet);
%{
deeplabNet_name = 'network_deeplabv3plus_resnet50.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "resnet50");
deeplabnet = trainNetWork(deeplabNet_name, deeplabnet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(deeplabnet);

deeplabNet_name = 'network_deeplabv3plus_resnet18.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "resnet18");
deeplabnet = trainNetWork(deeplabNet_name, deeplabnet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(deeplabnet);
%}
%{
%------------------------------
% 訓練 Segnet - 2 types 
%------------------------------

segnet_name = 'network_segnet_vgg16.mat';
segnet_lgraph = getSegNet(inputSize, numClasses, segnet_name, 'vgg16');
segnet = trainNetWork(segnet_name, segnet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(segnet);

segnet_name = 'network_segnet_vgg19.mat';
segnet_lgraph = getSegNet(inputSize, numClasses, segnet_name, 'vgg19');
segnet = trainNetWork(segnet_name, segnet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(segnet);

%------------------------------
% 訓練 U-Net - 1 type
%------------------------------
unet_name = 'network_unet_depth4.mat';
unet_lgraph = getUNet(inputSize, numClasses, unet_name);
unet = trainNetWork(unet_name, unet_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(unet);

%------------------------------
% 訓練 FCN  - 1 type
%------------------------------
fcn_name = 'network_fcn_32s.mat';
fcn_lgraph = getFCN(inputSize, numClasses, fcn_name, '32s'); % type: '8s' (default) | '16s' | '32s'
fcn = trainNetWork(fcn_name, fcn_lgraph, pximds, valPximds, run_date);
%analyzeNetwork(fcn);
%}

%% 訓練 function
function net=trainNetWork(netName, lgraph, augimdsTrain, valPximds, run_date)

result_dir = "Run_Result/";

if ~exist(fullfile(result_dir, run_date), 'dir')
    mkdir(fullfile(result_dir, run_date));
end

% 訓練設定選項
options = trainingOptions('sgdm',...
                          'Momentum', 0.9,...
                          'InitialLearnRate',0.001,...
                          'LearnRateSchedule','piecewise', ...
                          'ValidationData', valPximds, ...
                          'LearnRateDropFactor',0.1, ...
                          'LearnRateDropPeriod', 10, ...
                          'MaxEpochs',50, 'MiniBatchSize', 7,...
                          'VerboseFrequency',50, ...
                          'Plots','training-progress');

filename_txt = sprintf("TrainingLog_%s_%s.txt", netName ,run_date);
diary(fullfile(result_dir, run_date, filename_txt));
diary on;
fprintf('[INFO] Training network : %s ...\n',netName);
net = trainNetwork(augimdsTrain,lgraph,options);
diary off;
save(fullfile(result_dir, run_date, netName),'net');

netNOnly = netName(1: end-4); 
filename_gcf = sprintf("TrainingLog_%s.png", netNOnly);
filename_gcf = fullfile(result_dir, run_date, filename_gcf);
training_gcf = findall(groot, 'Type', 'Figure');
exportapp(training_gcf, filename_gcf);
%saveas(training_gcf, filename_gcf);
close(training_gcf);

end