%% 載入網路
network_dir = './networks/';
%----------------------
%       Deep Lab v3+
%----------------------

deeplabNet_inceptionresnetv2 = load(fullfile(network_dir,'network_deeplabv3plus_inceptionresnetv2_10folds_4.mat'),'netTransfer');
run_network(deeplabNet_inceptionresnetv2.netTransfer, 'deeplab_inceptionresnetv2');
deeplabNet_mobilev2 = load(fullfile(network_dir,'network_deeplabv3plus_mobilenetv2_10folds_3.mat'),'netTransfer');
run_network(deeplabNet_mobilev2.netTransfer, 'deeplab_mobilev2');
deeplabNet_xception = load(fullfile(network_dir,'network_deeplabv3plus_xception_10folds_3.mat'),'netTransfer');
run_network(deeplabNet_xception.netTransfer, 'deeplab_xception');
deeplabNet_resnet18 = load(fullfile(network_dir,'network_deeplabv3plus_resnet18_10folds_4.mat'),'netTransfer');
run_network(deeplabNet_resnet18.netTransfer, 'deeplab_resnet18');
deeplabNet_resnet50 = load(fullfile(network_dir,'network_deeplabv3plus_resnet50_10folds_5.mat'),'netTransfer');
run_network(deeplabNet_resnet50.netTransfer, 'deeplab_resnet50');

%----------------------
%       SegNet
%----------------------
%load net
segnet_vgg16 = load(fullfile(network_dir,'network_segnet_vgg16_10folds_3.mat'),'netTransfer');
run_network(segnet_vgg16.netTransfer, 'segnet_vgg16');
segnet_vgg19 = load(fullfile(network_dir,'network_segnet_vgg19_10folds_3.mat'),'netTransfer');
run_network(segnet_vgg19.netTransfer, 'segnet_vgg19');

%----------------------
%       U Net
%----------------------
unet = load(fullfile(network_dir,'network_unet_depth4_10folds_3.mat'),'netTransfer');
run_network(unet.netTransfer, 'unet_depth4');

%----------------------
%       FCN
%----------------------
fcn = load(fullfile(network_dir,'network_fcn_32s_10folds_2.mat'),'netTransfer');
run_network(fcn.netTransfer, 'fcn_32s');

%% funcitons 
function run_network(net, network_type)
    fprintf("[INFO] Start image segmentation with %s...\n", network_type);
    
    today = datetime('today');
    run_date = datestr(today, 'yyyy-mm-dd');
    
    % 開啟測試檔
    imgDir =fullfile(pwd,'Dataset/Test_image/');
    imgDirS = dir(fullfile(imgDir, '*.png'));
    imgDirN = natsortfiles({imgDirS.name});
    imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
    imds = imageDatastore(string(imgDirF));

    % 建立Label
    classes = [
        "Shadowing"
     %   "Thick Echogenic Halo"
        "Taller Than Wide"
        "Mircolobulation"
        "Hypoechogenicity"
        "Duct Extention"
        "Angular Margins"
        "Background"
     ];
    cmap = breathSegColorMap();
    
    originResultDir = strcat(pwd,"\Run_Result\" + run_date + "/Test_image/" + network_type + "_origin");
    if ~exist(originResultDir, 'dir')
        mkdir(originResultDir);
    end

    % 讀取測試圖片
    fileNum = length(imds.Files);
    for k = 1:fileNum
        [im, info] = readimage(imds, k);
        [filepath,name,ext] = fileparts(info.Filename);

        % 執行網路
        result = semanticseg(im, net);

        % 顯示結果
        output = labeloverlay(im, result, 'ColorMap', cmap, 'Transparency', 0.4);
        imshow(output);
        pixelLabelColorbar(cmap, classes);
        set(gcf, 'Position', [0 0 1920 1080]);
        
        filename = sprintf("Result_%s_%s_%s.png", network_type, name , run_date);
        writeDir = strcat(pwd,"\Run_Result\" + run_date + "/Test_image/" + network_type);
        if ~exist(writeDir, 'dir')
           mkdir(writeDir)
        end
        destination = fullfile(writeDir, filename);
        saveas(gcf,destination)
        close(gcf)
                
        filename = sprintf("Result_%s_%s_%s.png", network_type, name , run_date);
        output = labeloverlay(im, result, 'ColorMap', cmap, 'Transparency', 0.4);
        check_dst = fullfile(originResultDir, filename);
        imwrite(output,check_dst);
        
    end
    
    % 裁減圖片
    results = imageDatastore(writeDir);
    fileNum = length(results.Files);
    for k = 1: fileNum
        [im, info] = readimage(results, k);
        im2=imcrop(im,[700, 50, 1800, 1500]);  %裁減圖片
        imwrite(im2, info.Filename);
    end
    
    fprintf("[INFO] Image Segmentation completed!\n");
end
