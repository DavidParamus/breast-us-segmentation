%% Clear workspace and console
clc;
clear;
close;
diary off;

%% Input dataset 
% Image input size
inputSize = [360, 360, 3]; %[height, weight, channel]

% Create Class (label)
classes = [
    "Shadowing"
%    "Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
]; 
numClasses = length(classes);
cmap = breathSegColorMap();
labelIDs = breathSegLabels();

% ���J�V�m���Ϥ� 
imgDir =fullfile(pwd,'Dataset/Train_image_Aug/');
imgDirS = dir(fullfile(imgDir, '*.png'));
imgDirN = natsortfiles({imgDirS.name});
imgDirF = cellfun(@(n)fullfile(imgDir,n),imgDirN,'uni',0);
imds = imageDatastore(string(imgDirF));

% ���J�V�m�� Ground Truth 
labelDir = fullfile(pwd,'Dataset/Train_label_Aug/');
labelDirS = dir(fullfile(labelDir, '*.png'));
labelDirN = natsortfiles({labelDirS.name});
labelDirF = cellfun(@(n)fullfile(labelDir,n),labelDirN,'uni',0);
pxds = pixelLabelDatastore(labelDirF,classes,labelIDs);

today = datetime('today');
%run_date = datestr(today, 'yyyy-mm-dd');
run_date = '2021-07-10';

%% �V�m����
%------------------------------
% �V�m DeeplabNet v3 - 5 types
%------------------------------
%{
deeplabNet_name = 'network_deeplabv3plus_mobilenetv2.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "mobilenetv2");
trainNetWork(deeplabNet_name, deeplabnet_lgraph, imds, pxds, run_date);

deeplabNet_name = 'network_deeplabv3plus_inceptionresnetv2.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "inceptionresnetv2");
trainNetWork(deeplabNet_name, deeplabnet_lgraph, imds, pxds,run_date);



deeplabNet_name = 'network_deeplabv3plus_xception.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "xception");
trainNetWork(deeplabNet_name, deeplabnet_lgraph, imds, pxds, run_date);

deeplabNet_name = 'network_deeplabv3plus_resnet50.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "resnet50");
trainNetWork(deeplabNet_name, deeplabnet_lgraph, imds, pxds, run_date);


deeplabNet_name = 'network_deeplabv3plus_resnet18.mat';
deeplabnet_lgraph = getDeeplabNet(inputSize, numClasses, deeplabNet_name, "resnet18");
trainNetWork(deeplabNet_name, deeplabnet_lgraph, imds, pxds, run_date);


%------------------------------
% �V�m Segnet - 2 types 
%------------------------------

segnet_name = 'network_segnet_vgg16.mat';
segnet_lgraph = getSegNet(inputSize, numClasses, segnet_name, 'vgg16');
trainNetWork(segnet_name, segnet_lgraph, imds, pxds, run_date);

segnet_name = 'network_segnet_vgg19.mat';
segnet_lgraph = getSegNet(inputSize, numClasses, segnet_name, 'vgg19');
trainNetWork(segnet_name, segnet_lgraph, imds, pxds, run_date);

%------------------------------
% �V�m U-Net - 1 type
%------------------------------
unet_name = 'network_unet_depth4.mat';
unet_lgraph = getUNet(inputSize, numClasses, unet_name);
trainNetWork(unet_name, unet_lgraph, imds, pxds, run_date);
%}
%------------------------------
% �V�m FCN  - 1 type
%------------------------------
fcn_name = 'network_fcn_32s.mat';
fcn_lgraph = getFCN(inputSize, numClasses, fcn_name, '32s'); % type: '8s' (default) | '16s' | '32s'
trainNetWork(fcn_name, fcn_lgraph, imds, pxds, run_date);


%% �V�m function
function trainNetWork(netName, trainedNet, imds, pxds, run_date)

classes = [
    "Shadowing"
    %"Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
];
classNames = strrep(classes, "_","\_");

% Number of Images
num_images=length(imds.Files);

[~,netNOnly,~] = fileparts(netName);

fprintf("[INFO] Training network %s...\n", netNOnly);

writeDir = fullfile( "Run_Result",run_date, netNOnly);
if ~exist(fullfile(writeDir, 'Train_Log'), 'dir')
    mkdir(fullfile(writeDir, 'Train_Log'));
end

if ~exist(fullfile(writeDir, 'networks'), 'dir')
    mkdir(fullfile(writeDir, 'networks'));
end

if ~exist(fullfile(writeDir, 'Evaluation'), 'dir')
    mkdir(fullfile(writeDir, 'Evaluation'));
end

if ~exist(fullfile(writeDir, 'ROC_Curve'), 'dir')
    mkdir(fullfile(writeDir, 'ROC_Curve'));
end

% �V�m�]�w�ﶵ
options = trainingOptions('sgdm',...
                          'Momentum', 0.9,...
                          'InitialLearnRate',0.001,...
                          'LearnRateSchedule','piecewise', ...
                          'LearnRateDropFactor',0.1, ...
                          'LearnRateDropPeriod', 2, ...
                          'MaxEpochs',10, 'MiniBatchSize', 6,...
                          'VerboseFrequency',50, ...
                          'Plots','training-progress');

dsm_table = table;  % Save networks' DataSetMetrics 
num_folds = 10;
for fold_idx=1:num_folds
    
    fprintf('[INFO] Processing %s, %d among %d folds \n',netNOnly, fold_idx,num_folds);
    filename_txt = sprintf("TrainingLog_%s_%s_%dfold_%d.txt", netNOnly ,run_date, num_folds, fold_idx);
    diary(fullfile(writeDir, 'Train_Log', filename_txt));
    diary on;
        
    % Test Indices for current fold
    test_idx=fold_idx:num_folds:num_images;
    
    % Test cases for current fold
    imdsTest = subset(imds,test_idx);
    pxdsTest = subset(pxds,test_idx);
    
    % Train indices for current fold
    train_idx=setdiff(1:length(imds.Files),test_idx);
    
    % Train cases for current fold
    imdsTrain = subset(imds,train_idx);
    pxdsTrain = subset(pxds,train_idx);
    
    % Training
    pximds = pixelLabelImageDatastore(imdsTrain, pxdsTrain);
    netTransfer = trainNetwork(pximds, trainedNet, options);
    
    % Save the Independent CNN Architectures obtained for each Fold
    filenameNames = sprintf('%s_%dfolds_%d',netNOnly,num_folds, fold_idx);
    networkNames = fullfile(writeDir, "networks", filenameNames);
    save(networkNames,'netTransfer');   
    
    % Save training log graph
    filename_gcf = sprintf("TrainingLog_%s_fold-%d.png", netNOnly, fold_idx);
    filename_gcf = fullfile(writeDir, 'Train_Log', filename_gcf);
    training_gcf = findall(groot, 'Type', 'Figure');
    exportapp(training_gcf, filename_gcf);
    %saveas(training_gcf, filename_gcf);
    close(training_gcf);
    
    tic
    pxdsResults = semanticseg(imdsTest,netTransfer, ...
        'MiniBatchSize',5, ...
        'WriteLocation', tempdir,...
        'Verbose',false);
    runTime = toc;
    metrics = evaluateSemanticSegmentation(pxdsResults,pxdsTest,'Verbose',false);
    
    filename = sprintf('BSS_%s_%dfold_%d_ClassMetrics.csv', netNOnly, num_folds, fold_idx);
    writetable(metrics.ClassMetrics, fullfile(writeDir, 'Evaluation',filename));
    
    netName_table = table({filenameNames},  'VariableNames', {'Network'});
    runTime_table = table(runTime,  'VariableNames', {'Run Time'});
    dsm = metrics.DataSetMetrics;
    dsm = [netName_table, dsm, runTime_table];
    dsm_table = [dsm_table; dsm];
    
    % Confusion Matrix
    fig = figure(1);
    normConfMatData = metrics.NormalizedConfusionMatrix.Variables;
    h = heatmap(classNames,classNames,100*normConfMatData);
    h.XLabel = 'Predicted Class';
    h.YLabel = 'True Class';
    h.Title = 'Normalized Confusion Matrix (%)';
    filename = sprintf('BSS-%s-ConfusionMetrics.png', netNOnly);
    saveas(fig,fullfile(writeDir, 'Evaluation', filename));
    close(fig);

    diary off;
end

tableName_overall = sprintf("Overall-%s-DataTable.csv", netNOnly);
writeDir = fullfile(writeDir);
writetable(dsm_table, fullfile(writeDir, tableName_overall));
end
