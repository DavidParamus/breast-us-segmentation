% 載入當前路徑
nowDir = pwd;

% 載入圖片 
targetDir = strcat(nowDir, "\Database\Malignant\");
imgDir = fullfile(targetDir,'*'); %良性
imds = imageDatastore(imgDir);

% 輸出目標位置與載入 ground truth
writeDir = strcat(nowDir, "\Database\Malignant_label\");
checkDir = strcat(nowDir, "\Database\Malignant_Label_check\");
labelDir = strcat(nowDir, "\Ground Truth Labeler\Malignant\MalignantGTruthv2\");
labelS = dir(fullfile(labelDir, '*.png'));
labelN = natsortfiles({labelS.name});
labelF = cellfun(@(n)fullfile(labelDir,n),labelN,'uni',0);
labels = imageDatastore(string(labelF));

% 建立Label
classes = [  
    "Shadowing"
    "Thick_Echogenic_Halo"
    "Taller_Than_Wide"
    "Mircolobulation"
    "Hypoechogenicity"
    "Duct_Extention"
    "Angular_Margins"
    "Background"
    ];
cmap = breathSegColorMap();

% 輸出 Label 檔案
fileNum = length(imds.Files);
for k = 1:fileNum
    
    [originImg, originInfo] = readimage(imds, k);
    [filepath,name,ext] = fileparts(originInfo.Filename);
    labeledImg = readimage(labels, k);

    checkImg = labeloverlay(originImg, labeledImg, 'ColorMap', cmap, 'Transparency', 0.4);
    outputImg = label2rgb(labeledImg, cmap, [0,0,0], 'noshuffle');
    
    % show
    %subplot(1,2,1), imshow(originImg)
    %subplot(1,2,2), imshow(outputImg)
    %pause;
    % Write
    split_temp = split(name,"_");
    origin_name = string(split_temp(end));
    filename = sprintf("%05d_Malignant_label_%s.png", k, origin_name);
    wirte_dst = strcat(writeDir, filename);
    imwrite(outputImg,wirte_dst);
    check_dst = strcat(checkDir, filename);
    imwrite(checkImg,check_dst);
end