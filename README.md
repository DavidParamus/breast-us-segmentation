# Breath ultrasound semantic segmentation
## 乳房超音波語義分析

 ## Environment 開發環境
 * Matlab R2020a
   * Deep Learning Toolbox
   * Parallel computing toolbox
 * Python
   * opencv-python
   * Augmentor
   * Pillow

## Git Principle Git使用原則
 * 修改**.gitignore**要跟通知
 * 註解一定要寫，註解格式
   * \+ : add 新增功能。
   * \~ : fix 修復bug
   * \- : rm 移除檔案或者功能
   * \# : 修改既有功能
  
## Project 目錄
    /Dataset - 原始資料集
        /Human Cut Sample - 人工手動切割圖像
            /Benign   - 良性
            /Maligant - 惡性
        /Origin Sample - 原始圖像
            /ATL3000benign - 良性
            /ATL3000cancer - 惡性
            /ATL5000benign - 良性
            /ATL5000cancer - 惡性
        /Origin Sample_classification - 根據焦距分類好的圖像 
         (皆用Python自動切割，但Other內的不規則圖像需要手動切割)
            /ATL3000benign - 良性
                /3.0 
                /4.0 
                /4.8 
                /6.0
                /Other
            /ATL3000cancer - 惡性
                /3.0
                /4.0
                /4.8
                /6.0
                /Other
            /ATL5000benign - 良性
                /3.0
                /3.9_1
                /3.9_2
                /4.8_1
                /4.8_2
                /Other
            /ATL5000cancer - 惡性
                /3.0
                /3.9_1
                /3.9_2
                /4.8_1
                /4.8_2
    /Matlab - Train model and execute network (訓練網路與執行網路)
        /Database   - 資料庫 - 存放所有預處理完的資料
            /Benign          - 良性
            /Benign_label    - 良性 label
            /Malignant       - 惡性
            /Malignant_label - 惡性 label
            Breath_Benign_tb.csv
            Breath_Malignant_tb.csv
        /Dataset    - Matlab 執行用的訓練集與
            /Test_image         - 測試用圖片
            /Test_label         - 測試用 label
            /Train_image        - 訓練用圖片
            /Train_label        - 訓練用 label 
            /Train_image_Aug    - 訓練用圖片 (資料增強後)
            /Train_label_Aug    - 訓練用 label (資料增強後)
        /Ground Truth Labeler
            /Benign          - 儲存良性腫瘤之Session (注意: 原始讀像須在../Dataset之中)
            /Malignant       - 儲存惡性腫瘤之Session (注意: 原始讀像須在../Dataset之中)
        /Run_result - 執行結果
            /{依照日期區分}
        /natsortfiles        - 檔案排序用
        breathSegColorMap.m               - 乳房腫瘤 label 的 Color Map
        breathSegColorMap.m               - 乳房腫瘤 label
        getSegNet.m                       - 取得神經網路 SegNet
        OutputGroundTruthImage_Benign.m   - 輸出良性 imageLabeler 的結果
        OutputGroundTruthImage_Maligent.m - 輸出惡性 imageLabeler 的結果
        pixelLabelColorbar.m              - 輸出執行網路結果用的 Colorbar
        SegNet_Train_offical.m            - **主程式** 訓練網路
        SegNet_Run_offical.m              - **主程式** 執行網路
    /Python - 資料前處理 (Data processing)
        /.vscode                - VSCode執行環境設定
        /Dataset_cut            - 將原始資料進行切割，使其只保留超音波圖像部分
        /Dataset_resize         - 將原始資料進行resize，使其大小統一
        Cut.py                  - 圖像切割用
        DB_movement.py          - 對資料表進行CRUD (TODO: 功能不完整)
        Data_Augmentation.py    - 資料增強用
        Rename.py               - 重新命名並寫進資料表中(.csv)
        Resize.py               - 統一圖片大小
    READMD.md - 說明文件
    
## Program flow 執行流程
    ![流程圖](https://imgur.com/CqH1XL5)

